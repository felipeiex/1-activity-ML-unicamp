# -*- coding: utf-8 -*-
"""
#1 Activity
Created on Wed Mar 21 23:24:00 2018
@author: Reinaldo Felipe Soares Araujo
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.datasets import load_digits
from sklearn.model_selection import learning_curve
from sklearn.model_selection import ShuffleSplit

def plot_learning_curve(regr,title, x_tr, y_tr, ylim=None, cv=None,
                        n_jobs=-1, train_sizes=np.linspace(.9, 1.0, 5)):
        plt.figure()
        plt.title(title)
        if ylim is not None:
                plt.ylim(*ylim)
        plt.xlabel("Training examples")
        plt.ylabel("Score")
        rain_sizes, train_scores, test_scores = learning_curve(
                regr, x_tr, y_tr, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes)
        train_scores_mean = np.mean(train_scores, axis=1)
        train_scores_std = np.std(train_scores, axis=1)
        test_scores_mean = np.mean(test_scores, axis=1)
        test_scores_std = np.std(test_scores, axis=1)
        plt.grid()
        plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                         train_scores_mean + train_scores_std, alpha=0.1,
                         color="r")

        plt.plot(train_sizes, train_scores_mean, 'o-', color="r",
                 label="Training score")

        plt.legend(loc="best")
        return plt

#Loading the dataset
X = pd.read_csv('train.csv')

X_train = X.iloc[:, :-1].values
X_test = pd.read_csv('test.csv') 
X_test = X_test.iloc[:,:].values
y_train = X.iloc[:,60:61].values
y_test = pd.read_csv('test_target.csv') 
y_test = y_test.iloc[:,:].values

#Removing the first column.
X_train = X_train[:,1:]
X_test = X_test[:, 1:]

#Feature Scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)

#Fitting Multiple Linerar Regression to the Training Set
from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(X_train, y_train)
y_pred = regressor.predict(X_test)

mean_absolute_error(y_test, y_pred)

title = "Learning Curve"
plot_learning_curve(regressor, title, X_train, y_train, (0.02, 0.04))
plt.show()

"""
Result normalized: 3310
Result non-normalized: 3187
"""

#Polynomial Regresion
# Fitting Polynomial Regression to the dataset
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
poly_reg = PolynomialFeatures(degree = 2)
X_train_poly = poly_reg.fit_transform(X_train)
X_test_poly = poly_reg.transform(X_test)
lin_reg_2  = LinearRegression()
lin_reg_2.fit(X_train_poly, y_train)
y_poly_pred = lin_reg_2.predict(X_test_poly)
mean_absolute_error(y_test, y_poly_pred)

title = "Learning Curve"
plot_learning_curve(poly_reg, title, X_train, y_train, (0, 1))
plt.show()
"""
Result normalized: 179522743349
Resultado non-normalized: 25238

"""
#Logistic Regression
from sklearn.linear_model import LogisticRegression
logreg = LogisticRegression(n_jobs = -1, max_iter = 4)
logreg.fit(X_train, y_train)
y_pred = logreg.predict(X_test)
mean_absolute_error(y_test, y_pred)
"""
1 Interection = 2576
2 Interaction = 2586

Normalizada
1 Interaction =  2576

2 Interaction = 2586

3 Interaction = 2585

5 Interaction = 2576
"""
title = "Learning Curve"
plot_learning_curve(logreg, title, X_train, y_train, (0.05, 0.07))
plt.show()
